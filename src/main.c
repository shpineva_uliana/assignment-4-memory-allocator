#define _GNU_SOURCE

#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 4096



void debug(const char* fmt, ... );

#define get_header(mem) \
    ((struct block_header*) (((uint8_t*) (mem)) - offsetof(struct block_header, contents)))

void simple_allocation() {
    debug("Test 1: simple allocation\n");
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        debug("Test 1 failed: heap is not initialized");
        return;
    }
    debug("\nHeap initialized: %d bytes\n", HEAP_SIZE);
    debug_heap(stderr, heap);

    void *block = _malloc(2048);
    if (!block) {
        debug("Test 1 failed: memory is not allocated");
        return;
    }
    debug("\nHeap after allocation:\n");
    debug_heap(stderr, heap);

    _free(block);
    debug("\nHeap after free:\n");
    debug_heap(stderr, heap);
    heap_term();
    debug("\nTest 1 passed!\n\n");
}

void free_one_block() {
    debug("\nTest 2: free one block\n");
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        debug("Test 2 failed: heap is not initialized");
        return;
    }
    debug("\nHeap initialized: %d bytes\n", HEAP_SIZE);
    debug_heap(stderr, heap);

    void *block1 = _malloc(2048);
    void *block2 = _malloc(1024);
    if (!block1 || !block2) {
        debug("Test 2 failed: memory is not allocated");
        return;
    }

    debug("\nHeap after allocation:\n");
    debug_heap(stderr, heap);

    _free(block1);
    assert(get_header(block1)->is_free);
    debug("\nHeap after free block #1:\n");
    debug_heap(stderr, heap);
    _free(block2);
    assert(get_header(block2)->is_free);
    heap_term();
    debug("\nTest 2 passed!\n\n");
}

void free_two_blocks() {
    debug("\nTest 3: free two blocks\n");
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        debug("Test 3 failed: heap is not initialized");
        return;
    }
    debug("\nHeap initialized: %d bytes\n", HEAP_SIZE);
    debug_heap(stderr, heap);

    void *block1 = _malloc(1024);
    void *block2 = _malloc(1024);
    void *block3 = _malloc(1024);
    void *block4 = _malloc(1024);
    void *block5 = _malloc(1024);
    if (!block1 || !block2 || !block3 || !block4 || !block5) {
        debug("Test 3 failed: memory is not allocated");
        return;
    }

    debug("\nHeap after allocation:\n");
    debug_heap(stderr, heap);

    _free(block2);
    _free(block4);
    assert(!get_header(block1)->is_free);
    assert(get_header(block2)->is_free);
    assert(!get_header(block3)->is_free);
    assert(get_header(block4)->is_free);
    assert(!get_header(block5)->is_free);
    debug("\nHeap after free block #2 and #4:\n");
    debug_heap(stderr, heap);
    _free(block1);
    debug("\nHeap after free block #1:\n");
    debug_heap(stderr, heap);
    _free(block3);
    _free(block5);
    heap_term();
    debug("\nTest 3 passed!\n\n");
}

void memory_expansion() {
    debug("\nTest 4: memory expansion\n");
    struct region *heap = heap_init(1);
    if (!heap) {
        debug("Test 4 failed: heap is not initialized");
        return;
    }

    debug("\nHeap initialized\n");
    debug_heap(stderr, heap);
    size_t heap_size1 = heap->size;

    void *block1 = _malloc(2 * 4096);
    debug("\nHeap after allocation:\n");
    debug_heap(stderr, heap);
    void *block2 = _malloc(4 * 4096);
    if (!block1 || !block2) {
        debug("Test 4 failed: memory is not allocated");
    }
    debug("\nHeap after allocation:\n");
    debug_heap(stderr, heap);

    size_t heap_size2 = heap->size;
    assert(heap_size1 < heap_size2);

    _free(block1);
    debug("\nHeap after free block #1:\n");
    debug_heap(stderr, heap);
    _free(block2);
    debug("\nHeap after free block #2:\n");
    debug_heap(stderr, heap);
    heap_term();
    debug("\nTest 4 passed!\n\n");
}

void filled_region_expansion() {
    debug("\nTest 5: filled region expansion\n");
    void *heap = heap_init(0);
    if (!heap) {
        debug("Test 5 failed: heap is not initialized");
        return;
    }
    debug("\nHeap initialized:\n");
    debug_heap(stderr, heap);

    void *fixed_block = mmap(HEAP_START, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , -1, 0 );
    if(fixed_block == MAP_FAILED) {
        debug("Could not mmap");
    }
    assert(fixed_block != MAP_FAILED);
    debug("Mapped at %p", fixed_block);

    debug("\nHeap:\n");
    debug_heap(stderr, heap);



    void *block1 = _malloc(HEAP_SIZE);
    debug("\nHeap after first allocation:\n");
    debug_heap(stderr, heap);


    void *block2 = _malloc(HEAP_SIZE);
    if (!block1 || !block2) {
        debug("Test 5 failed: memory is not allocated");
    }
    debug("\nHeap after second allocation:\n");
    debug_heap(stderr, heap);
    assert(block1 != fixed_block && block2 != fixed_block);

    _free(block1);
    _free(block2);
    debug("\nHeap after free block #1 and #2:\n");
    debug_heap(stderr, heap);
    heap_term();
    debug("\nTest 5 passed!\n\n");
}

int main() {
    simple_allocation();
    free_one_block();
    free_two_blocks();
    memory_expansion();
    filled_region_expansion();
    return 0;
}
